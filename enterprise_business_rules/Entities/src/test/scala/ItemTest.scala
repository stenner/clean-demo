import Entities.Item
import org.scalatest.{GivenWhenThen, FeatureSpec}


class ItemTest extends FeatureSpec with GivenWhenThen{
  feature("Item"){
    scenario("Create new sword"){
      Given("the name 'sword' and the price 5")
      val name = "sword"
      val price = 5
      When("creating the item")
      val sword = Item(name, price)
      Then("the item should have be named 'sword'")
      assert(sword.name == name)
      And("have a price of 5")
      assert(sword.price == price)
    }
  }
}
