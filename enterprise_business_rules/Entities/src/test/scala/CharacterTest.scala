import Entities.{Wallet, Character, Item}
import org.scalatest.{FeatureSpec, GivenWhenThen}

import scala.collection.mutable


class CharacterTest extends FeatureSpec with GivenWhenThen{
  feature("Character"){
    scenario("Create new character"){
      Given("the name 'Svend' and a wallet with 8 gold in it")
      val name = "Svend"
      val gold = 8
      val moneyPouch = Wallet(gold)
      val backpack = new mutable.ListBuffer[Item]
      When("creating the character")
      val svend = Character(name, moneyPouch, backpack)
      Then("he should be called Svend")
      assert(svend.name == name)
      And("have a wallet with 8 gold in it")
      assert(svend.wallet.gold == gold)
      And("have an empty inventory")
      assert(svend.inventory.isEmpty)
    }
  }
}
