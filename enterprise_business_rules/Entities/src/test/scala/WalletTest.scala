import Entities.{Wallet, Item}
import org.scalatest.{FeatureSpec, GivenWhenThen}


class WalletTest extends FeatureSpec with GivenWhenThen{
  feature("Wallet"){
    scenario("Create new wallet"){
      Given("8 gold")
      val gold = 8
      When("creating the wallet")
      val wallet = Wallet(gold)
      Then("the wallet should contain 8 gold")
      assert(wallet.gold == gold)
    }
  }
}
