package Entities

import scala.collection.mutable


case class Character(name: String, wallet: Wallet, inventory: mutable.ListBuffer[Item])
