package DataSource

case class TransactionResult (wasSuccessful: Boolean)
