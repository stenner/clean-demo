import sbt._

object Dependencies {
  val scalatestVersion = "2.2.6"
  val scalatest = "org.scalatest" %% "scalatest" % scalatestVersion % "test"
  val testDependencies: Seq[ModuleID] = Seq(scalatest)
  val serviceDependencies: Seq[ModuleID] = Seq()
}