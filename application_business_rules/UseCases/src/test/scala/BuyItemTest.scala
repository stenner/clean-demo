import Entities.{Character, Item, Wallet}
import UseCases.{BuyItem, CommandResult, DefaultItemsHandler, Invoker}
import org.scalatest.{FeatureSpec, GivenWhenThen}

import scala.collection.mutable


class BuyItemTest extends FeatureSpec with GivenWhenThen{
  feature("Buy Item Use Case"){
    scenario("Buy sword"){
      Given("a blacksmith with a sword and 1000 gold")
      val (sword: Item, blacksmith: Character, hero: Character) = createBuyItemUseCase
      When("hero buys the sword")
      val buyResult = Invoker.invoke(BuyItem(sword, 5, hero, blacksmith, DefaultItemsHandler))
      Then("the transaction should be successful")
      assertBuyItemUseCase(sword, blacksmith, hero, buyResult)
    }
  }

  def assertBuyItemUseCase(sword: Item, blacksmith: Character, hero: Character, buyResult: CommandResult): Unit = {
    assert(buyResult.wasSuccessful)
    And("the hero should have 0 gold")
    assert(hero.wallet.gold == 0)
    And("the blacksmith should have 1005 gold")
    assert(blacksmith.wallet.gold == 1005)
    And("the sword should be in the inventory of the hero")
    assert(hero.inventory.head.equals(sword))
    And("the sword should not be in the inventory of the blacksmith")
    assert(blacksmith.inventory.isEmpty)
  }

  def createBuyItemUseCase: (Item, Character, Character) = {
    val sword = Item("sword", 5)
    val blacksmithInventory = new mutable.ListBuffer[Item]
    blacksmithInventory += sword
    val blacksmith = Character("Blacksmith", Wallet(1000), blacksmithInventory)
    And("a hero with 5 gold and an empty inventory")
    val heroInventory = new mutable.ListBuffer[Item]
    val hero = Character("Hero", Wallet(5), heroInventory)
    (sword, blacksmith, hero)
  }
}