import Dependencies._

//scalaVersion := "2.11.7"
//scapegoatVersion := "1.1.0"
wartremoverErrors ++= Warts.unsafe

lazy val entities: Project = (project in file("enterprise_business_rules/Entities")).
  settings(Common.settings: _*).
  settings(libraryDependencies ++= testDependencies
  )

lazy val useCases = (project in file("application_business_rules/UseCases")).
  settings(Common.settings: _*).
  settings(libraryDependencies ++= testDependencies).
  dependsOn(entities)

lazy val controllers = (project in file("interface_adapters/Controllers")).
  settings(Common.settings: _*).
  settings(libraryDependencies ++= testDependencies).
  dependsOn(useCases)

lazy val gateways = (project in file("interface_adapters/Gateways")).
  settings(Common.settings: _*).
  settings(libraryDependencies ++= testDependencies).
  dependsOn(useCases)

lazy val presenters = (project in file("interface_adapters/Presenters")).
  settings(Common.settings: _*).
  settings(libraryDependencies ++= testDependencies).
  dependsOn(useCases)

lazy val consoleUI = (project in file("frameworks_and_drivers/ConsoleUI")).
  settings(Common.settings: _*).
  settings(libraryDependencies ++= testDependencies).
  dependsOn(controllers, presenters)

lazy val playFramework = (project in file("frameworks_and_drivers/PlayFramework")).
  settings(Common.settings: _*).
  settings(libraryDependencies ++= testDependencies).
  dependsOn(controllers, presenters)

lazy val scalike = (project in file("frameworks_and_drivers/Scalike")).
  settings(Common.settings: _*).
  settings(libraryDependencies ++= testDependencies).
  dependsOn(gateways)
