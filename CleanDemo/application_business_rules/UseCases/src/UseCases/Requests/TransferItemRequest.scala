package UseCases.Requests

import Entities.{Character, Item}

case class TransferItemRequest(item: Item, buyer: Character, seller: Character)
