package UseCases.Requests

import Entities.{Character, Item}

case class BuyItemRequest(item: Item, buyer: Character, seller: Character)
