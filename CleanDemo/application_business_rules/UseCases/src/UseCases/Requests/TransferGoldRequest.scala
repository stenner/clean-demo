package UseCases.Requests

import Entities.Character

case class TransferGoldRequest (gold: Int, buyer: Character, seller: Character)