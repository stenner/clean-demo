package UseCases


trait UseCase [Result, Argument] {
  def execute(arg: Argument): Result
}

