package UseCases

import DataSource.ItemsDataSource
import Entities.{Item,Character}

case class ViewInventoryUseCase(itemsDataSource: ItemsDataSource) extends UseCase[List[Item], Character] {
  override def execute(arg: Character): List[Item] = {
    itemsDataSource.getItemsForCharacter(arg)
  }
}






