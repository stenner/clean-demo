package UseCases


trait UseCaseArgumentLess[Result] {
  def execute: Result
}
