package UseCases

import DataSource.ItemsDataSource
import UseCases.Requests.TransferItemRequest

case class TransferItemUseCase(itemsDataSource: ItemsDataSource) extends UseCase[Unit, TransferItemRequest] {
  override def execute(arg: TransferItemRequest): Unit = {
    itemsDataSource.TransferItem(arg.item, arg.buyer, arg.seller)
  }
}
