package UseCases

import DataSource.ItemsDataSource
import UseCases.Requests.TransferGoldRequest

case class TransferGoldUseCase(itemsDataSource: ItemsDataSource) extends UseCase[Unit, TransferGoldRequest] {
  override def execute(arg: TransferGoldRequest): Unit = {
    itemsDataSource.TransferGold(arg.gold, arg.buyer, arg.seller)
  }
}
